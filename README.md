
 
## Documentation
Installation, customization and other useful articles: https://akveo.github.io/ng2-admin/

## Based on
Angular 2+, Angular CLI, Bootstrap 4, and lots of awesome modules and plugins

## BrowserStack
This project runs its tests on multiple desktop and mobile browsers using [BrowserStack](http://www.browserstack.com).

<img src="https://cloud.githubusercontent.com/assets/131406/22254249/534d889e-e254-11e6-8427-a759fb23b7bd.png" height="40" />

