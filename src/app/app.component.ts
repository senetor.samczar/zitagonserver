import { Component, ViewContainerRef } from "@angular/core";
import * as $ from "jquery";
import { Router } from "@angular/router";

import { GlobalState } from "./global.state";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "./theme/services";
import { BaThemeConfig } from "./theme/theme.config";
import { layoutPaths } from "./theme/theme.constants";
import { AF } from "./providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: "app",
  styleUrls: ["./app.component.scss"],
  template: `
    <main [class.menu-collapsed]="isMenuCollapsed" baThemeRun>
      <div class="additional-bg"></div>

      <router-outlet></router-outlet>
    </main>
  `
})
export class App {
  isMenuCollapsed: boolean = false;
  isLoggedIn: boolean;
  constructor(
    public router: Router,
    public afService: AF,
    private _state: GlobalState,
    private _imageLoader: BaImageLoaderService,
    private _spinner: BaThemeSpinner,
    private viewContainerRef: ViewContainerRef,
    private themeConfig: BaThemeConfig
  ) {
    themeConfig.config();

    this._loadImages();

    this._state.subscribe("menu.isCollapsed", isCollapsed => {
      this.isMenuCollapsed = isCollapsed;
    });

    this.afService.afAuth.authState.subscribe((user: firebase.User) => {
      if (!user) {
        this.router.navigate(["login"]);
      } else {
        this.afService.af
          .object("/users/" + firebase.auth().currentUser.uid)
          .subscribe(res => {
            if (res.role == 2) {
              this.router.navigate(["pages"]);
            } else {
              this.router.navigate(["pages"]);

              //this.router.navigate(['login']);
            }
          });
      }
    });

    /*
this.afService.afAuth.subscribe(
      (auth) => {
        if( firebase.auth() == null ) 
        {
          this.isLoggedIn = false;
          this.router.navigate(['login']);
        }
        else 
        {

              this.afService.af.object('/users/' + auth.uid).subscribe(res => {
                  if ( res.role == 2 ) {

                    if( auth.google ) 
          {
            this.afService.displayName = auth.google.displayName;
            this.afService.email = auth.google.email;
          }
          else 
          {
            this.afService.displayName = auth.auth.email;
            this.afService.email = auth.auth.email;
          }
          this.isLoggedIn = true;
          //this.router.navigate(['pages']);

                  }

                  else {
                    this.afService.signOut();
                              this.router.navigate(['login']);

                  }
              });

          
        }
      }
    );*/
  }

  public ngAfterViewInit(): void {
    // hide spinner once all loaders are completed
    BaThemePreloader.load().then(values => {
      this._spinner.hide();
    });
  }

  private _loadImages(): void {
    // register some loaders
    BaThemePreloader.registerLoader(
      this._imageLoader.load("/assets/img/sky-bg.jpg")
    );
  }
}
