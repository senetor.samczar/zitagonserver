import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';

import { Dashboard } from './dashboard.component';
import { routing } from './dashboard.routing';

import { PopularApp } from './popularApp';
import { PieChart } from './pieChart';
import { TrafficChart } from './trafficChart';
import { UsersMap } from './usersMap';
import { LineChart } from './lineChart';
import { Feed } from './feed';
import { feedTrackPopular } from './feedTrackPopular';
import { feedAlbum } from './feedAlbum';
import { feedAlbumPopular } from './feedAlbumPopular';
import { feedArtist } from './feedArtist';
import { feedArtistPopular } from './feedArtistPopular';
import { Todo } from './todo';
import { Calendar } from './calendar';
import { CalendarService } from './calendar/calendar.service';
import { LineChartService } from './lineChart/lineChart.service';
import { PieChartService } from './pieChart/pieChart.service';
import { TodoService } from './todo/todo.service';
import { TrafficChartService } from './trafficChart/trafficChart.service';
import { UsersMapService } from './usersMap/usersMap.service';
import {ReversePipe} from 'ngx-pipes/src/app/pipes/array/reverse';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
  ],
  declarations: [
    PopularApp,
    PieChart,
    TrafficChart,
    UsersMap,
    LineChart,
    Feed,
    feedTrackPopular,
    feedAlbum,
    feedAlbumPopular,
    feedArtist,
    feedArtistPopular,
    Todo,
    Calendar,
    Dashboard,
    ReversePipe
  ],
  providers: [
    CalendarService,
    LineChartService,
    PieChartService,
    TodoService,
    TrafficChartService,
    UsersMapService,
  ],
})
export class DashboardModule {}
