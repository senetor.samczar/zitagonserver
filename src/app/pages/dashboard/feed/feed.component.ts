import { Component } from '@angular/core';
import { AF } from '../../../providers/af';


@Component({
  selector: 'feed',
  templateUrl: './feed.html',
  styleUrls: ['./feed.scss']
})
export class Feed {

  public feed: any[] = [];

  constructor( public af: AF) {

 this.af.af.list('/tracks', { preserveSnapshot: true, query: {
    orderByChild: 'timestamp',
    limitToLast: 50
  } })
    .subscribe(snapshots => {
      this.feed = [];
snapshots.forEach(track => {
this.feed.push(
{
     type: 'text-message',
      author: track.val().name,
      surname: track.val().albumName,
      header: 'Added new Song to album : ',
      text: track.val().artistName,
      preview: track.val().albumArt,
      link: track.val().url,
      time: track.val().timestamp,
      ago: Date.now() - track.val().timestamp,
      expanded: false,
    },
    );
}); 

 this.feed.reverse();

    });




  }

  ngOnInit(){
  }

  expandMessage (message){
    message.expanded = !message.expanded;
  }

  private _loadFeed() {
  }
}
