import { Component } from '@angular/core';
import { AF } from '../../../providers/af';


@Component({
  selector: 'feedArtist',
  templateUrl: './feed.html',
  styleUrls: ['./feed.scss']
})
export class feedArtist {

  public feed: any[] = [];

  constructor( public af: AF) {

 this.af.af.list('/artists', { preserveSnapshot: true,query:{
    orderByChild: 'timestamp',
    limitToLast: 50
  } })
    .subscribe(snapshots => {
      this.feed = [];
snapshots.forEach(track => {
this.feed.push(
{
     type: 'text-message',
      author: track.val().name,
      surname: ' ',
      header: '',
      text: track.val().genre,
      preview: track.val().image,
      cover: track.val().cover,
      link: ' ',
      time: track.val().timestamp,
      ago: Date.now() - track.val().timestamp,
      expanded: false,
    },
    );
});

 this.feed.reverse();

    });




  }

  ngOnInit(){
  }

  expandMessage (message){
    message.expanded = !message.expanded;
  }

  private _loadFeed() {
  }
}
