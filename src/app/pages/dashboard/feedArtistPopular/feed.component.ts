import { Component } from '@angular/core';
import { AF } from '../../../providers/af';


@Component({
  selector: 'feedArtistPopular',
  templateUrl: './feed.html',
  styleUrls: ['./feed.scss']
})
export class feedArtistPopular {

  public feed: any[] = [];

  constructor( public af: AF) {

this.af.af.list('/popularArtist', { preserveSnapshot: true, query: {
  orderByChild: 'count',
  limitToLast: 50
}}).subscribe(track => {
      this.feed = [];
 track.forEach(a => {

  this.af.af.list('/artists/', { preserveSnapshot: true, query: {
      orderByKey: true,
    equalTo: a.key
  }} ).subscribe( art => {
    art.forEach( n => {
this.feed.push(
{
     type: 'text-message',
      author: n.val().name,
      surname: ' ',
      header: '',
      text: n.val().genre,
      preview: n.val().image,
      cover: n.val().cover,
      link: ' ',
      time: n.val().timestamp,
      ago: Date.now() - n.val().timestamp,
      expanded: false,
    },
    );
    });
     

  });


 });

});



  }
 expandMessage (message) {
    message.expanded = !message.expanded;
  }

}
