import { Component } from '@angular/core';
import { AF } from '../../../providers/af';
import {ReversePipe} from 'ngx-pipes/src/app/pipes/array/reverse';


@Component({
  selector: 'feedTrackPopular',
  templateUrl: './feed.html',
  styleUrls: ['./feed.scss'],
    providers: [ReversePipe],

})
export class feedTrackPopular {

  public feed: any[] = [];

  constructor( private reversePipe: ReversePipe, public af: AF) {





this.af.af.list('/popularTrack', { preserveSnapshot: true, query: {
  orderByChild: 'count',
  limitToLast: 50
}}).subscribe(track => {
      this.feed = [];
 track.forEach(a => {

  this.af.af.list('/tracks/', { preserveSnapshot: true, query: {
      orderByKey: true,
    equalTo: a.key
  }} ).subscribe( art => {
    art.forEach( n => {

this.feed.push(
{
     type: 'text-message',
      author: n.val().name,
      surname: n.val().albumName,
      header: '',
      text: n.val().artistName,
      preview: n.val().albumArt,
      link: n.val().url,
      time: n.val().timestamp,
      ago: Date.now() - n.val().timestamp,
      expanded: false,
    },
    );
    });
     

  });


 });

});



  




  }

  ngOnInit(){
    this.feed.reverse();
  }

  expandMessage (message){
    message.expanded = !message.expanded;
  }

  
}
