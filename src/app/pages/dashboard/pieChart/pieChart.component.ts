import { Component } from "@angular/core";

import { PieChartService } from "./pieChart.service";
import { AF } from "../../../providers/af";

import "easy-pie-chart/dist/jquery.easypiechart.js";
import { BaThemeConfigProvider, colorHelper } from "../../../theme";
import "rxjs/add/operator/take";

@Component({
  selector: "pie-chart",
  templateUrl: "./pieChart.html",
  styleUrls: ["./pieChart.scss"]
})
// TODO: move easypiechart to component
export class PieChart {
  public charts: any[] = [];
  private _init = false;

  constructor(
    private _pieChartService: PieChartService,
    private _baConfig: BaThemeConfigProvider,
    public af: AF
  ) {
    const pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    this.charts = [
      {
        color: pieColor,
        description: "dashboard.num_songs",
        stats: 0
      },
      {
        color: pieColor,
        description: "dashboard.num_artists",
        stats: 0
      },
      {
        color: pieColor,
        description: "dashboard.num_albums",
        stats: 0
      },
      {
        color: pieColor,
        description: "dashboard.num_plays",
        stats: 0
      },
      {
        color: pieColor,
        description: "Played in 24h",
        stats: 0
      },
      {
        color: pieColor,
        description: "Played Today",
        stats: 0
      },
      {
        color: pieColor,
        description: "Played This Week",
        stats: 0
      },
      {
        color: pieColor,
        description: "Played in 30 days",
        stats: 0
      },

      {
        color: pieColor,
        description: "Popular Song",
        stats: ""
      },

      {
        color: pieColor,
        description: "Popular Album",
        stats: ""
      },

      {
        color: pieColor,
        description: "Popular Artist",
        stats: ""
      },

      {
        color: pieColor,
        description: "Total Users",
        stats: ""
      }
    ];

    this.af.af
      .list("/users", { preserveSnapshot: true })
      .subscribe(snapshots => {
        this.charts[11].stats = snapshots.length;
      });

    this.af.af
      .list("/popularArtist", {
        preserveSnapshot: true,
        query: {
          orderByChild: "count",
          limitToLast: 1
        }
      })
      .subscribe(artist => {
        artist.forEach(a => {
          this.af.af
            .list("/artists", {
              preserveSnapshot: true,
              query: {
                orderByKey: true,
                equalTo: a.key
              }
            })
            .subscribe(art => {
              art.forEach(n => {
                this.charts[10].stats = n.val().name;
              });
            });
        });
      });

    this.af.af
      .list("/popularAlbum", {
        preserveSnapshot: true,
        query: {
          orderByChild: "count",
          limitToLast: 1
        }
      })
      .subscribe(album => {
        album.forEach(a => {
          this.af.af
            .list("/albums/", {
              preserveSnapshot: true,
              query: {
                orderByKey: true,
                equalTo: a.key
              }
            })
            .subscribe(art => {
              art.forEach(n => {
                this.charts[9].stats = n.val().name;
                this.charts[9].subtitle = n.val().artistName;
              });
            });
        });
      });

    this.af.af
      .list("/popularTrack", {
        preserveSnapshot: true,
        query: {
          orderByChild: "count",
          limitToLast: 1
        }
      })
      .subscribe(track => {
        track.forEach(a => {
          this.af.af
            .list("/tracks/", {
              preserveSnapshot: true,
              query: {
                orderByKey: true,
                equalTo: a.key
              }
            })
            .subscribe(art => {
              art.forEach(n => {
                this.charts[8].stats = n.val().name;
                this.charts[8].subtitle =
                  n.val().artistName + " - " + n.val().albumName;
              });
            });
        });
      });

    this.af.af
      .list("/tracks", { preserveSnapshot: true })
      .subscribe(snapshots => {
        this.charts[0].stats = snapshots.length;
      });

    this.af.af
      .list("/artists", { preserveSnapshot: true })
      .subscribe(snapshots => {
        this.charts[1].stats = snapshots.length;
      });

    this.af.af
      .list("/albums", { preserveSnapshot: true })
      .subscribe(snapshots => {
        this.charts[2].stats = snapshots.length;
      });

    this.af.af
      .list("/popularTrack", { preserveSnapshot: true })
      .subscribe(snapshots => {
        this.charts[3].stats = 0;
        snapshots.forEach(play => {
          this.charts[3].stats = this.charts[3].stats + play.val().count;
        });
      });

    this.af.af.list("/tracks", { preserveSnapshot: true }).subscribe(tracks => {
      this.charts[4].stats = 0;
      this.charts[5].stats = 0;
      this.charts[6].stats = 0;
      this.charts[7].stats = 0;
      tracks.forEach(track => {
        this.af.af
          .list("/popularTrack/" + track.key + "/played", {
            preserveSnapshot: true
          })
          .take(1)
          .subscribe(plays => {
            plays.forEach(play => {
              if (
                play.val().timespan > this.af.getTodayStart() &&
                play.val().timespan < this.af.getTodayEnd()
              ) {
                this.charts[5].stats = this.charts[5].stats + 1;
              }

              if (
                play.val().timespan > this.af.getYesterdayStart() &&
                play.val().timespan < this.af.getYesterdayEnd()
              ) {
                this.charts[4].stats = this.charts[4].stats + 1;
              }

              if (
                play.val().timespan > this.af.getWeekStart() &&
                play.val().timespan < this.af.getWeekEnd()
              ) {
                this.charts[6].stats = this.charts[6].stats + 1;
              }

              if (
                play.val().timespan > this.af.getMonthStart() &&
                play.val().timespan < this.af.getMonthEnd()
              ) {
                this.charts[7].stats = this.charts[7].stats + 1;
              }
            });
          });
      });
    });
  }

  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {
    jQuery(".chart").each(function() {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: "easeOutBounce",
        onStep: function(from, to, percent) {
          jQuery(this.el)
            .find(".percent")
            .text(Math.round(percent));
        },
        barColor: jQuery(this).attr("data-rel"),
        trackColor: "rgba(0,0,0,0)",
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: "round"
      });
    });
  }

  private _updatePieCharts() {
    let getRandomArbitrary = (min, max) => {
      return Math.random() * (max - min) + min;
    };

    jQuery(".pie-charts .chart").each(function(index, chart) {
      jQuery(chart)
        .data("easyPieChart")
        .update(getRandomArbitrary(55, 90));
    });
  }
}
