import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";

import { AlertService } from "../../../../../_services/index";

@Component({
  selector: "inputs",
  templateUrl: "./inputs.html"
})
export class Inputs {
  public artists: any[] = [];
  private uploadTask: firebase.storage.UploadTask;

  public artistss: FirebaseListObservable<any>;
  public countrys: FirebaseListObservable<any>;
  public searchTerm: any;
  public curartist: any = null;
  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.artistss = this.af.af.list("/artists", { preserveSnapshot: true });
    this.countrys = this.af.af.list("/countrys", { preserveSnapshot: true });
  }

  delete(key) {
    this.af.af
      .list("/artists")
      .remove(key)
      .then(x => {
        // alert("Deleted successfully");

        this.alertService.error("Album Deleted ");
        this.curartist = null;
        this.artists = [];
        this.searchTerm = "";
      });
  }

  updateImage(event) {
    // this.spinner.show();
    this.artists = [];
    this.searchTerm = "";
    const data = this.curartist;
    const storageRef = firebase.storage().ref();

    const success = false;

    const selectedFile = event.srcElement.files[0];
    const af = this.af.af;
    const path = `/${data.name}/image`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/artists/" + data.key)
            .update({
              image: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Artist Info Updated ");
            });
        });
    });
  }

  updateCover(event) {
    // this.spinner.show();

    this.artists = [];
    this.searchTerm = "";
    const data = this.curartist;

    const storageRef = firebase.storage().ref();
    this.searchTerm = "";
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/${data.name}/cover`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/artists/" + data.key)
            .update({
              cover: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Artist Info Updated ");
            });
        });
    });
  }

  updateInfo() {
    // this.spinner.show();

    const data = this.curartist;
    this.af.af
      .object("/artists/" + data.key)
      .update({
        name: data.name,
        countryName: data.countryName
        // genre: data.genre
      })
      .then(x => {
        this.af.af
          .list("/albums", {
            preserveSnapshot: true,
            query: {
              orderByChild: "artist",
              equalTo: data.key
            }
          })
          .subscribe(albums => {
            albums.forEach(album => {
              this.af.af.object("/albums/" + album.key).update({
                artistName: data.name
              });
            });
          });

        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "artist",
              equalTo: data.key
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                artistName: data.name
              });
            });
          });
      })
      .then(x => {
        // window.location.reload();
        this.alertService.success("Artist Info Updated ");
      });
  }

  close() {
    this.curartist = null;
  }
  edit(artist) {
    this.curartist = {
      key: artist.key,
      name: artist.val().name,
      genre: artist.val().genre,
      image: artist.val().image,
      cover: artist.val().cover,
      countryName: artist.val().countryName
    };
  }

  search(ev) {
    this.artists = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 3) {
      this.artistss.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
          ) {
            this.artists.push(item);
          }
        });
      });
    }
  }
}
