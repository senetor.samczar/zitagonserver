import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";
import { AlertService } from "../../../../../_services/index";

@Component({
  selector: "inputsalbum",
  templateUrl: "./inputsalbum.html"
})
export class Inputsalbum {
  public albums: any[] = [];
  public albumss: FirebaseListObservable<any>;
  public searchTerm: any;
  public curalbum: any = null;
  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.albumss = this.af.af.list("/albums", { preserveSnapshot: true });
  }

  delete(key) {
    this.af.af
      .list("/albums")
      .remove(key)
      .then(x => {
        // alert("Deleted successfully");
        this.alertService.error("Album Deleted ");
        this.curalbum = null;
        this.albums = [];
        this.searchTerm = "";
      });
  }

  updateImage(event) {
    // this.spinner.show();

    this.albums = [];
    this.searchTerm = "";
    const data = this.curalbum;
    const storageRef = firebase.storage();

    const success = false;
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/${data.artistName}/${data.name}/image`;
    const iRef = storageRef.ref(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/albums/" + data.key)
            .update({
              image: url
            })
            .then(x => {
              this.af.af
                .list("/tracks", {
                  preserveSnapshot: true,
                  query: {
                    orderByChild: "album",
                    equalTo: data.key
                  }
                })
                .subscribe(tracks => {
                  tracks.forEach(track => {
                    this.af.af.object("/tracks/" + track.key).update({
                      albumArt: url
                    });
                  });
                });
            })
            .then(x => {
              //   window.location.reload();
              this.alertService.success("Track Updated ");
            });
        });
    });
  }

  updateInfo() {
    // this.spinner.show();

    const data = this.curalbum;
    this.af.af
      .object("/albums/" + data.key)
      .update({
        name: data.name,
        release: data.release
      })
      .then(x => {
        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "album",
              equalTo: data.key
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                albumName: data.name
              });
            });
          });
      })
      .then(x => {
        this.albums = [];
        this.curalbum = null;
        this.searchTerm = "";
        // this.spinner.hide();
      });
  }

  close() {
    this.curalbum = null;
  }
  edit(album) {
    this.curalbum = {
      key: album.key,
      name: album.val().name,
      artistName: album.val().artistName,
      release: album.val().release,
      image: album.val().image
    };
  }

  search(ev) {
    this.albums = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 3) {
      this.albumss.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1 ||
            item
              .val()
              .artistName.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
          ) {
            this.albums.push(item);
          }
        });
      });
    }
  }
}
