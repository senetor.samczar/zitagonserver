import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";

import { AlertService } from "../../../../../_services/index";

@Component({
  selector: "inputscountry",
  templateUrl: "./inputscountry.html"
})
export class Inputscountry {
  public countrys: any[] = [];
  private uploadTask: firebase.storage.UploadTask;

  public countryss: FirebaseListObservable<any>;
  public artistss: FirebaseListObservable<any>;
  public regions: FirebaseListObservable<any>;
  public searchTerm: any;
  public curcountry: any = null;
  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.countryss = this.af.af.list("/countrys", { preserveSnapshot: true });
    this.regions = this.af.af.list("/regions", { preserveSnapshot: true });
  }

  delete(key) {
    this.af.af
      .list("/countrys")
      .remove(key)
      .then(x => {
        // alert("Deleted successfully");
        this.alertService.error("Country Deleted ");
        this.curcountry = null;
        this.countrys = [];
        this.searchTerm = "";
      });
  }

  updateImage(event) {
    // this.spinner.show();
    this.countrys = [];
    this.searchTerm = "";
    const data = this.curcountry;
    const storageRef = firebase.storage().ref();

    const success = false;

    const selectedFile = event.srcElement.files[0];
    const af = this.af.af;
    const path = `/${data.name}/image`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/countrys/" + data.key)
            .update({
              image: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Country Updated ");
            });
        });
    });
  }

  updateCover(event) {
    // this.spinner.show();

    this.countrys = [];
    this.searchTerm = "";
    const data = this.curcountry;

    const storageRef = firebase.storage().ref();
    this.searchTerm = "";
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/${data.name}/cover`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/countrys/" + data.key)
            .update({
              cover: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Country Updated ");
            });
        });
    });
  }

  updateInfo() {
    // this.spinner.show();

    const data = this.curcountry;
    this.af.af
      .object("/countrys/" + data.key)
      .update({
        name: data.name,
        countryRegion: data.regionName
      })
      .then(x => {
        this.af.af
          .list("/albums", {
            preserveSnapshot: true,
            query: {
              orderByChild: "country",
              equalTo: data.key
            }
          })
          .subscribe(albums => {
            albums.forEach(album => {
              this.af.af.object("/albums/" + album.key).update({
                countryName: data.name,
                countryRegion: data.regionName
              });
            });
          });

        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "country",
              equalTo: data.key
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                countryName: data.name
              });
            });
          });
      })
      .then(x => {
        // window.location.reload();
        this.alertService.success("Country Updated ");
      });
  }

  close() {
    this.curcountry = null;
  }
  edit(country) {
    this.curcountry = {
      key: country.key,
      name: country.val().name,
      image: country.val().image,
      cover: country.val().cover,
      countryRegion: country.val().regionName
    };
  }

  search(ev) {
    this.countrys = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 3) {
      this.countryss.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
          ) {
            this.countrys.push(item);
          }
        });
      });
    }
  }
}
