import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";
import { AlertService } from "../../../../../_services/index";

@Component({
  selector: "inputsmood",
  templateUrl: "./inputsmood.html"
})
export class Inputsmood {
  public moods: FirebaseListObservable<any>;
  public searchTerm: any;
  public curmood: any = null;
  public oldmood: any = null;
  public moodsList: any[] = [];
  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.moods = this.af.af.list("/moods", {
      preserveSnapshot: true
    });
  }

  delete(key) {
    this.af.af
      .list("/moods")
      .remove(key)
      .then(x => {
        // alert("Deleted successfully");
        this.alertService.error("Album Deleted ");
        this.curmood = null;
        this.moodsList = [];
        this.searchTerm = "";
      });
  }

  updateInfo() {
    this.spinner.show();

    const data = this.curmood;
    this.af.af
      .object("/moods/" + data.key)
      .update({
        name: data.name
      })
      .then(x => {
        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "mood",
              equalTo: this.oldmood.name
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                mood: data.name
              });
            });
          });

        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "mood2",
              equalTo: this.oldmood.name
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                mood2: data.name
              });
            });
          });

        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "mood3",
              equalTo: this.oldmood.name
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                mood3: data.name
              });
            });
          });

        // window.location.reload();
        this.alertService.success("Mood Updated ");
      });
  }

  close() {
    this.curmood = null;
  }
  edit(mood) {
    this.curmood = null;
    setTimeout(() => {
      this.curmood = {
        key: mood.key,
        name: mood.val().name,
        image: mood.val().image
      };
      this.oldmood = {
        key: mood.key,
        name: mood.val().name,
        image: mood.val().image
      };
    }, 200);
  }

  search(ev) {
    this.moodsList = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 3) {
      this.moods.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
          ) {
            this.moodsList.push(item);
          }
        });
      });
    }
  }

  updateMoodImage(event) {
    this.spinner.show();

    this.moodsList = [];
    this.searchTerm = "";
    const data = this.curmood;
    const storageRef = firebase.storage().ref();

    const success = false;
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/moods/${data.name}`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/moods/" + data.key)
            .update({
              image: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Mood Updated ");
            });
        });
    });
  }
}
