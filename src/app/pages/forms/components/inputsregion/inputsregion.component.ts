import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";

import { AlertService } from "../../../../../_services/index";

@Component({
  selector: "inputsregion",
  templateUrl: "./inputsregion.html"
})
export class Inputsregion {
  public regions: any[] = [];
  private uploadTask: firebase.storage.UploadTask;

  public regionss: FirebaseListObservable<any>;
  public searchTerm: any;
  public curregion: any = null;
  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.regionss = this.af.af.list("/regions", { preserveSnapshot: true });
  }

  delete(key) {
    this.af.af
      .list("/regions")
      .remove(key)
      .then(x => {
        // alert("Deleted successfully");
        this.alertService.error("Album Deleted ");
        this.curregion = null;
        this.regions = [];
        this.searchTerm = "";
      });
  }

  updateImage(event) {
    // this.spinner.show();
    this.regions = [];
    this.searchTerm = "";
    const data = this.curregion;
    const storageRef = firebase.storage().ref();

    const success = false;

    const selectedFile = event.srcElement.files[0];
    const af = this.af.af;
    const path = `/${data.name}/image`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/regions/" + data.key)
            .update({
              image: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Region Updated ");
            });
        });
    });
  }

  updateCover(event) {
    // this.spinner.show();

    this.regions = [];
    this.searchTerm = "";
    const data = this.curregion;

    const storageRef = firebase.storage().ref();
    this.searchTerm = "";
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/${data.name}/cover`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/regions/" + data.key)
            .update({
              cover: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Region Updated ");
            });
        });
    });
  }

  updateInfo() {
    // this.spinner.show();

    const data = this.curregion;
    this.af.af
      .object("/regions/" + data.key)
      .update({
        name: data.name
      })
      .then(x => {
        this.af.af
          .list("/albums", {
            preserveSnapshot: true,
            query: {
              orderByChild: "region",
              equalTo: data.key
            }
          })
          .subscribe(albums => {
            albums.forEach(album => {
              this.af.af.object("/albums/" + album.key).update({
                regionName: data.name
              });
            });
          });

        this.af.af
          .list("/tracks", {
            preserveSnapshot: true,
            query: {
              orderByChild: "region",
              equalTo: data.key
            }
          })
          .subscribe(tracks => {
            tracks.forEach(track => {
              this.af.af.object("/tracks/" + track.key).update({
                regionName: data.name
              });
            });
          });
      })
      .then(x => {
        // window.location.reload();
        this.alertService.success("Region Updated ");
      });
  }

  close() {
    this.curregion = null;
  }
  edit(region) {
    this.curregion = {
      key: region.key,
      name: region.val().name,
      image: region.val().image,
      cover: region.val().cover
    };
  }

  search(ev) {
    this.regions = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 3) {
      this.regionss.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
          ) {
            this.regions.push(item);
          }
        });
      });
    }
  }
}
