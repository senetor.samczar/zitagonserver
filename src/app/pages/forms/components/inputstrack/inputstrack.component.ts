import { Component, Inject } from "@angular/core";
import { AF } from "../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../theme/services";
import { AlertService } from "../../../../../_services/index";

import { Router } from "@angular/router";
import { all } from "q";

@Component({
  selector: "inputstrack",
  templateUrl: "./inputstrack.html"
})
export class Inputstrack {
  public tracks: any[] = [];
  public trackss: FirebaseListObservable<any>;
  public moods: FirebaseListObservable<any>;
  public searchTerm: any;
  public curtrack: any = null;
  dropdownList = [];
  dropdownListMode = [];
  selectedItems = [];
  dropdownSettings = {};
  dropdownSettingsFeaturing = {};
  selectedFeaturing = [];

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    public router: Router,
    private alertService: AlertService
  ) {
    this.moods = this.af.af.list("/moods", {
      // preserveSnapshot: true
    });
    this.trackss = this.af.af.list("/tracks", {
      preserveSnapshot: true
    });

    this.af.af.list("/artists").subscribe(v => {
      v.forEach(d => {
        this.dropdownList.push({ id: d.$key, name: d.name });
      });
    });

    this.moods.subscribe(c => {
      c.forEach(element => {
        this.dropdownListMode.push({ id: element.$key, name: element.name });
      });
    });

    this.dropdownSettings = {
      placeholder: "Select mood",
      singleSelection: false,
      idField: "id",
      textField: "name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: all,
      allowSearchFilter: true
    };

    this.dropdownSettingsFeaturing = {
      placeholder: "Select Artists",
      singleSelection: false,
      idField: "id",
      textField: "name",
      selectAllText: "Select Alls",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: all,
      allowSearchFilter: true
    };
  }

  delete(key) {
    this.af.af
      .list("/tracks")
      .remove(key)
      .then(x => {
        alert("Deleted successfully");
        this.alertService.error("Track deleted ");
        this.curtrack = null;
        this.tracks = [];
        this.searchTerm = "";
      });
  }

  updateInfo() {
    const data = this.curtrack;
    const datakey = this.curtrack.key;
    // this.spinner.show();

    if (data.chosen == undefined) {
      data.chosen = false;
    }
    this.af.af
      .object("/tracks/" + datakey)
      .update({
        name: data.name,
        mood: data.mood,
        featuring: data.featuring,
        collaborations: data.collaborations,
        chosen: data.chosen,
        video: data.video
      })
      .then(x => {
        if (data.chosen == true) {
          this.af.af
            .list("tracks", {
              preserveSnapshot: true,
              query: {
                orderByChild: "album",
                equalTo: data.album
              }
            })
            .subscribe(track => {
              track.forEach(tr => {
                if (tr.key != datakey) {
                  this.af.af.object("/tracks/" + tr.key).update({
                    chosen: false
                  });
                }
              });
            });
        }
        this.alertService.success("Track Edited ");
        // window.location.reload();
      });
  }

  close() {
    this.curtrack = null;
  }
  edit(track) {
    this.curtrack = null;
    setTimeout(() => {
      this.curtrack = {
        key: track.key,
        name: track.val().name,
        chosen: track.val().chosen,
        mood: track.val().mood,
        collaborations: track.val().collaborations,
        featuring: track.val().featuring,
        artistName: track.val().artistName,
        albumName: track.val().albumName,
        album: track.val().album,
        artist: track.val().artist,
        url: track.val().url,
        video: track.val().video,
        albumArt: track.val().albumArt
      };
      if (
        this.curtrack.mood == null ||
        this.curtrack.mood == undefined ||
        this.curtrack.mood == ""
      ) {
        this.curtrack.mood = 0;
      }
      if (
        this.curtrack.collaborations == null ||
        this.curtrack.collaborations == undefined ||
        this.curtrack.collaborations == ""
      ) {
        this.curtrack.collaborations = 0;
      }
      if (
        this.curtrack.featuring == null ||
        this.curtrack.featuring == undefined ||
        this.curtrack.featuring == ""
      ) {
        this.curtrack.featuring = 0;
      }
    }, 200);
  }

  search(ev) {
    this.tracks = [];

    const val = ev.target.value;

    if (val != "" && val.length >= 1) {
      this.trackss.subscribe(items => {
        items.forEach(item => {
          if (
            item
              .val()
              .name.toLowerCase()
              .indexOf(val.toLowerCase()) > -1 ||
            item
              .val()
              .artistName.toLowerCase()
              .indexOf(val.toLowerCase()) > -1 ||
            item
              .val()
              .albumName.toLowerCase()
              .indexOf(val.toLowerCase()) > -1
            // (item.val().featuring.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            // (item.val().collaborations.toLowerCase().indexOf(val.toLowerCase()) > -1)
          ) {
            this.tracks.push(item);
          }
        });
      });
    }
  }

  updateTrack(event) {
    // this.spinner.show();

    this.tracks = [];
    this.searchTerm = "";
    const data = this.curtrack;
    const storageRef = firebase.storage().ref();

    const success = false;
    // This currently only grabs item 0, TODO refactor it to grab them all
    const selectedFile = event.srcElement.files[0];
    // Make local copies of services because "this" will be clobbered
    const af = this.af.af;
    const path = `/${data.artistName}/${data.albumName}/${data.name}`;
    const iRef = storageRef.child(path);
    iRef.put(selectedFile).then(snapshot => {
      firebase
        .storage()
        .ref(path)
        .getDownloadURL()
        .then(url => {
          this.af.af
            .object("/tracks/" + data.key)
            .update({
              url: url
            })
            .then(x => {
              // window.location.reload();
              this.alertService.success("Track Added ");
            });
        });
    });
  }
}
