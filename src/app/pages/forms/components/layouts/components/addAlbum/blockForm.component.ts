import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";
import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addAlbum-form",
  templateUrl: "./blockForm.html"
})
export class addAlbum {
  public artists: FirebaseListObservable<any>;
  curartist: any;
  genre: any = "rock";
  genres: any[] = [];
  albumName: any;
  url: any;
  release: any;

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.curartist = {
      $key: 0
    };
    this.artists = this.af.af.list("/artists");
  }

  upload() {
    if (
      this.albumName == "" ||
      this.albumName == null ||
      this.albumName == undefined
    ) {
      alert("Album Name Cannot be Empty");
    } else if (
      this.release == "" ||
      this.release == null ||
      this.release == undefined
    ) {
      alert("Release year Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("image")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("image")).files[0] == null
    ) {
      alert("Album Image Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("image")).files[0]
      ]) {
        const path = `/${this.curartist.name}/${this.albumName}`;
        const iRef = storageRef.child(path);
        const albumName = this.albumName;
        const release = this.release;
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              this.af.af.list("/albums").push({
                artist: this.curartist.$key,
                artistName: this.curartist.name,
                name: albumName,
                timestamp: Date.now(),
                image: url,
                path: path,
                release: release,
                approved: true,
                provider: "Admin"
              });
            });
          this.albumName = "";
          this.release = "";
          // ( < HTMLInputElement > document.getElementById('image')).files[0] = null;
          (<HTMLInputElement>document.getElementById("image")).value = "";
          this.alertService.success("Album Added ");
          // this.spinner.hide();
        });
      }
    }
  }
}
