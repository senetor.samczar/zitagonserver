import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";
import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addArtist-form",
  templateUrl: "./blockForm.html"
})
export class addArtist {
  artistName: any = "";
  url: any;
  key: any = "";
  public countrys: FirebaseListObservable<any>;
  artistCountry: any = 0;

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    public alertService: AlertService
  ) {
    this.countrys = this.af.af.list("/countrys", { preserveSnapshot: true });

    console.log(this.countrys);
  }

  upload() {
    if (
      this.artistName == "" ||
      this.artistName == null ||
      this.artistName == undefined
    ) {
      alert("Artist Name Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("artistImage")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("artistImage")).files[0] ==
        null
    ) {
      alert("Artist Image Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("artistCover")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("artistCover")).files[0] ==
        null
    ) {
      alert("Artist Cover Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      const success = false;

      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("artistImage")).files[0]
      ]) {
        const af = this.af.af;
        const path = `/${this.artistName}/image`;
        const iRef = storageRef.child(path);
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              for (const selectedFile2 of [
                (<HTMLInputElement>document.getElementById("artistCover"))
                  .files[0]
              ]) {
                // Make local copies of services because "this" will be clobbered
                const af2 = this.af.af;
                const path2 = `/${this.artistName}/cover`;
                const iRef2 = storageRef.child(path2);
                const artistName = this.artistName;
                const artistCountry = this.artistCountry;
                iRef2.put(selectedFile2).then(snapshot2 => {
                  this.af.af.app
                    .storage()
                    .ref(path2)
                    .getDownloadURL()
                    .then(url2 => {
                      af.list("/artists").push({
                        name: artistName,
                        timestamp: Date.now(),
                        image: url,
                        imagePath: path,
                        cover: url2,
                        coverPath: path2,
                        countryName: artistCountry,
                        approved: true,
                        provider: "Admin"
                      });
                    });
                  // (<HTMLInputElement>document.getElementById('artistImage')).files[0] = null;
                  // (<HTMLInputElement>document.getElementById('artistCover')).files[0] = null;
                  (<HTMLInputElement>(
                    document.getElementById("artistImage")
                  )).value = "";
                  (<HTMLInputElement>(
                    document.getElementById("artistCover")
                  )).value = "";
                  this.artistName = "";
                  this.artistCountry = "";
                  // alert("Data Added");
                  this.alertService.success("Artist Added");
                  // this.spinner.hide();
                });
              }
            });
        });
      }
    }
  }
}
