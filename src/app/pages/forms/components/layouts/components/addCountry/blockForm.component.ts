import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";
import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addCountry-form",
  templateUrl: "./blockForm.html"
})
export class addCountry {
  countryName: any = "";
  url: any;
  key: any = "";
  public regions: FirebaseListObservable<any>;
  countryRegion: any = 0;
  // regionName: any;

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    public alertService: AlertService
  ) {
    this.regions = this.af.af.list("/regions", { preserveSnapshot: true });

    console.log(this.regions);
  }

  upload() {
    if (
      this.countryName == "" ||
      this.countryName == null ||
      this.countryName == undefined
    ) {
      alert("Country Name Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("countryImage")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("countryImage")).files[0] ==
        null
    ) {
      alert("Country Image Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("countryCover")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("countryCover")).files[0] ==
        null
    ) {
      alert("Country Cover Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      const success = false;

      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("countryImage")).files[0]
      ]) {
        const af = this.af.af;
        const path = `/${this.countryName}/image`;
        const iRef = storageRef.child(path);
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              for (const selectedFile2 of [
                (<HTMLInputElement>document.getElementById("countryCover"))
                  .files[0]
              ]) {
                // Make local copies of services because "this" will be clobbered
                const af2 = this.af.af;
                const path2 = `/${this.countryName}/cover`;
                const iRef2 = storageRef.child(path2);
                const countryName = this.countryName;
                const countryRegion = this.countryRegion;
                iRef2.put(selectedFile2).then(snapshot2 => {
                  this.af.af.app
                    .storage()
                    .ref(path2)
                    .getDownloadURL()
                    .then(url2 => {
                      af.list("/countrys").push({
                        name: countryName,
                        timestamp: Date.now(),
                        image: url,
                        imagePath: path,
                        cover: url2,
                        coverPath: path2,
                        countryRegion: countryRegion,
                        // add country here

                        approved: true,
                        provider: "Admin"
                      });
                    });
                  // (<HTMLInputElement>document.getElementById('countryImage')).files[0] = null;
                  // (<HTMLInputElement>document.getElementById('countryCover')).files[0] = null;
                  (<HTMLInputElement>(
                    document.getElementById("countryImage")
                  )).value = "";
                  (<HTMLInputElement>(
                    document.getElementById("countryCover")
                  )).value = "";
                  this.countryName = "";
                  this.countryRegion = "";
                  // alert("Data Added");
                  // this.spinner.hide();
                  this.alertService.success("Country Added");
                });
              }
            });
        });
      }
    }
  }
}
