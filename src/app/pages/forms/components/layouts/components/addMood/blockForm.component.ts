import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";
import { Alert } from "../../../../../../../../node_modules/@types/selenium-webdriver";

import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addMood-form",
  templateUrl: "./blockForm.html"
})
export class addMood {
  moodName: any;
  url: any;
  key: any = "";

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    public alertService: AlertService
  ) {}

  upload() {
    if (
      this.moodName == "" ||
      this.moodName == null ||
      this.moodName == undefined
    ) {
      alert("Mood Name Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("moodImage")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("moodImage")).files[0] == null
    ) {
      alert("Mood Image Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      const success = false;
      // This currently only grabs item 0, TODO refactor it to grab them all
      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("moodImage")).files[0]
      ]) {
        // Make local copies of services because "this" will be clobbered
        const af = this.af.af;
        const name = this.moodName;
        const path = `/moods/${this.moodName}`;
        const iRef = storageRef.child(path);
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              af.list("/moods").push({
                name: name,
                image: url,
                path: path,
                approved: true,
                provider: "Admin"
              });
            });
          this.moodName = "";
          // (<HTMLInputElement>(
          //   document.getElementById("moodImage")
          // )).files[0] = undefined;
          (<HTMLInputElement>document.getElementById("moodImage")).value = "";
          this.alertService.success("Mood Added");
          // alert("Added Data");
          // this.spinner.hide();
        });
      }
    }
  }
}
