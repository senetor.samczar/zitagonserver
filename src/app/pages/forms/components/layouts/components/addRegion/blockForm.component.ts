import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";

import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addRegion-form",
  templateUrl: "./blockForm.html"
})
export class addRegion {
  regionName: any = "";
  url: any;
  key: any = "";

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    public alertService: AlertService
  ) {}

  upload() {
    if (
      this.regionName == "" ||
      this.regionName == null ||
      this.regionName == undefined
    ) {
      alert("Region Name Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("regionImage")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("regionImage")).files[0] ==
        null
    ) {
      alert("Region Image Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("regionCover")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("regionCover")).files[0] ==
        null
    ) {
      alert("Region Cover Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      const success = false;

      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("regionImage")).files[0]
      ]) {
        const af = this.af.af;
        const path = `/${this.regionName}/image`;
        const iRef = storageRef.child(path);
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              for (const selectedFile2 of [
                (<HTMLInputElement>document.getElementById("regionCover"))
                  .files[0]
              ]) {
                // Make local copies of services because "this" will be clobbered
                const af2 = this.af.af;
                const path2 = `/${this.regionName}/cover`;
                const iRef2 = storageRef.child(path2);
                const regionName = this.regionName;
                iRef2.put(selectedFile2).then(snapshot2 => {
                  this.af.af.app
                    .storage()
                    .ref(path2)
                    .getDownloadURL()
                    .then(url2 => {
                      af.list("/regions").push({
                        name: regionName,
                        timestamp: Date.now(),
                        image: url,
                        imagePath: path,
                        cover: url2,
                        coverPath: path2,
                        // add region here

                        approved: true,
                        provider: "Admin"
                      });
                    });
                  // (<HTMLInputElement>document.getElementById('regionImage')).files[0] = null;
                  // (<HTMLInputElement>document.getElementById('regionCover')).files[0] = null;
                  (<HTMLInputElement>(
                    document.getElementById("regionImage")
                  )).value = "";
                  (<HTMLInputElement>(
                    document.getElementById("regionCover")
                  )).value = "";
                  this.regionName = "";
                  this.alertService.success("Region Added");
                  // alert("Data Added");
                  // this.spinner.hide();
                });
              }
            });
        });
      }
    }
  }
}
