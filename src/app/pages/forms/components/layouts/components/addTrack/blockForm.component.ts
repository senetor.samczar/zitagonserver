import { Component, Inject } from "@angular/core";
import { AF } from "../../../../../../providers/af";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from "../../../../../../theme/services";
import { all } from "q";
// https://github.com/cornflourblue/angular2-alert-notifications
import { AlertService } from "../../../../../../../_services/index";

@Component({
  selector: "addTrack-form",
  templateUrl: "./blockForm.html"
})
export class addTrack {
  public artists: FirebaseListObservable<any>;
  public albumsdb: FirebaseListObservable<any>;
  public albums: any[] = [];
  curartist: any = [];
  curftartist: any = [];
  curcoartist: any = [];
  curalbum: any;

  trackName: any;
  trackMood: any = 0;
  trackVideo: any;
  moods: FirebaseListObservable<any>;
  url: any;
  key: any = "";

  dropdownList = [];
  dropdownListMode = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
    public af: AF,
    public spinner: BaThemeSpinner,
    private alertService: AlertService
  ) {
    this.curartist = { $key: 0 };
    this.artists = this.af.af.list("/artists");
    this.albumsdb = this.af.af.list("/albums", { preserveSnapshot: true });
    this.moods = this.af.af.list("/moods");
    this.congifureDropDown();
  }

  clear() {
    this.alertService.clear();
  }

  change(event) {
    this.albums = [];
    this.albumsdb.subscribe(snapshots => {
      snapshots.forEach(art => {
        if (art.val().artist == this.curartist.$key) {
          this.albums.push(art);
        }
      });

      this.curalbum = this.albums[0];
    });
  }

  congifureDropDown() {
    this.moods.subscribe(d => {
      d.forEach(v => {
        this.dropdownListMode.push({ id: v.$key, name: v.name });
      });
    });

    this.artists.subscribe(d => {
      d.forEach(v => {
        this.dropdownList.push({ id: v.$key, name: v.name });
      });

      this.dropdownSettings = {
        placeholder: "Select Artists",
        singleSelection: false,
        idField: "id",
        textField: "name",
        selectAllText: "Select All",
        unSelectAllText: "UnSelect All",
        itemsShowLimit: all,
        allowSearchFilter: true
      };
    });
  }
  onItemSelect(event: any) {
    console.log(event);
    console.log(this.curartist);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  upload() {
    if (
      this.trackVideo == "" ||
      this.trackVideo == null ||
      this.trackVideo == undefined
    ) {
      this.trackVideo = false;
    }

    if (
      this.trackName == "" ||
      this.trackName == null ||
      this.trackName == undefined
    ) {
      alert("Track Name Cannot be Empty");
    } else if (
      (<HTMLInputElement>document.getElementById("track")).files[0] ==
        undefined ||
      (<HTMLInputElement>document.getElementById("track")).files[0] == null
    ) {
      alert("Track File Cannot be Empty");
    } else {
      // this.spinner.show();

      const storageRef = this.af.af.app.storage().ref();

      const success = false;
      // This currently only grabs item 0, TODO refactor it to grab them all
      for (const selectedFile of [
        (<HTMLInputElement>document.getElementById("track")).files[0]
      ]) {
        // Make local copies of services because "this" will be clobbered
        const af = this.af.af;
        const path = `/${this.curartist.name}/${this.curalbum.val().name}/${
          this.trackName
        }`;
        const iRef = storageRef.child(path);
        const trackMood = this.trackMood || 0;
        const trackName = this.trackName;
        const trackVideo = this.trackVideo;
        iRef.put(selectedFile).then(snapshot => {
          this.af.af.app
            .storage()
            .ref(path)
            .getDownloadURL()
            .then(url => {
              af.list("/tracks").push({
                album: this.curalbum.key,
                albumArt: this.curalbum.val().image,
                albumName: this.curalbum.val().name,
                artist: this.curartist.$key,
                artistName: this.curartist.name,
                featuring: this.curftartist,
                collaborations: this.curcoartist,
                mood: trackMood,
                name: trackName,
                video: trackVideo,
                timestamp: Date.now(),
                url: url,
                path: path,
                chosen: true,
                approved: true,
                provider: "Admin"
              });

              this.af.af
                .list("tracks", {
                  preserveSnapshot: true,
                  query: {
                    orderByChild: "album",
                    equalTo: this.curalbum.key
                  }
                })
                .subscribe(track => {
                  track.forEach(tr => {
                    if (
                      tr.val().name != this.trackName &&
                      tr.val().album == this.curalbum.key
                    ) {
                      this.af.af.object("/tracks/" + tr.key).update({
                        chosen: false
                      });
                    }
                  });
                });
            });
          this.trackName = [];
          this.trackVideo = "";

          (<HTMLInputElement>document.getElementById("track")).value = "";

          this.alertService.success("Track Added ");
          // this.spinner.hide();
        });
      }
    }
  }
}
