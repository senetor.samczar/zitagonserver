import { Component } from "@angular/core";

@Component({
  selector: "forms",
  template: `
    <alert></alert>
    <router-outlet></router-outlet>
  `
})
export class Forms {
  constructor() {}
}
