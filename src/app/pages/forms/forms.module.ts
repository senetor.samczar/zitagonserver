import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule as AngularFormsModule } from "@angular/forms";
import { AppTranslationModule } from "../../app.translation.module";
import { NgaModule } from "../../theme/nga.module";
import { NgbRatingModule } from "@ng-bootstrap/ng-bootstrap";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
// import alert service and component
import { AlertComponent } from "../../../_directives/index";
import { AlertService } from "../../../_services/index";

import { routing } from "./forms.routing";

import { Forms } from "./forms.component";
import { Inputs } from "./components/inputs";
import { Inputstrack } from "./components/inputstrack";
import { Inputsmood } from "./components/inputsmood";
import { Inputsalbum } from "./components/inputsalbum";
import { Layouts } from "./components/layouts";
import { Inputscountry } from "./components/inputscountry";
import { Inputsregion } from "./components/inputsregion";

import { StandardInputs } from "./components/inputs/components/standardInputs";
import { ValidationInputs } from "./components/inputs/components/validationInputs";
import { GroupInputs } from "./components/inputs/components/groupInputs";
import { CheckboxInputs } from "./components/inputs/components/checkboxInputs";
import { Rating } from "./components/inputs/components/ratinginputs";
import { SelectInputs } from "./components/inputs/components/selectInputs";

import { InlineForm } from "./components/layouts/components/inlineForm";
import { BlockForm } from "./components/layouts/components/blockForm";
import { addAlbum } from "./components/layouts/components/addAlbum";
import { addMood } from "./components/layouts/components/addMood";
import { addArtist } from "./components/layouts/components/addArtist";
import { addCountry } from "./components/layouts/components/addCountry";
import { addRegion } from "./components/layouts/components/addRegion";
import { addTrack } from "./components/layouts/components/addTrack";
import { HorizontalForm } from "./components/layouts/components/horizontalForm";
import { BasicForm } from "./components/layouts/components/basicForm";
import { WithoutLabelsForm } from "./components/layouts/components/withoutLabelsForm";

@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    AppTranslationModule,
    NgaModule,
    NgbRatingModule,
    routing,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    Layouts,
    Inputs,
    Inputstrack,
    Inputsalbum,
    Inputsregion,
    Inputsmood,
    Inputscountry,
    Forms,
    StandardInputs,
    ValidationInputs,
    GroupInputs,
    CheckboxInputs,
    Rating,
    SelectInputs,
    InlineForm,
    BlockForm,
    addAlbum,
    addTrack,
    addArtist,
    addCountry,
    addRegion,
    addMood,
    HorizontalForm,
    BasicForm,
    WithoutLabelsForm,
    AlertComponent
  ],
  providers: [
    // include alert service in app module providers
    AlertService
  ]
})
export class FormsModule {}
