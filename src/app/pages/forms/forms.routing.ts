import { Routes, RouterModule } from "@angular/router";

import { Forms } from "./forms.component";
import { Inputs } from "./components/inputs/inputs.component";
import { Inputsmood } from "./components/inputsmood/inputsmood.component";
import { Inputsalbum } from "./components/inputsalbum/inputsalbum.component";
import { Inputstrack } from "./components/inputstrack/inputstrack.component";
import { Layouts } from "./components/layouts/layouts.component";
import { Inputsregion } from "./components/inputsregion/inputsregion.component";
import { Inputscountry } from "./components/inputscountry/inputscountry.component";

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: "",
    component: Forms,
    children: [
      { path: "inputs", component: Inputs },
      { path: "inputsalbum", component: Inputsalbum },
      { path: "inputstrack", component: Inputstrack },
      { path: "inputsmood", component: Inputsmood },
      { path: "layouts", component: Layouts },
      { path: "inputsregion", component: Inputsregion },
      { path: "inputscountry", component: Inputscountry }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
