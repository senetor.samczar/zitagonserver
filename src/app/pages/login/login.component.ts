import { Component } from "@angular/core";
import {
  FormGroup,
  AbstractControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { AF } from "../../providers/af";
import { Router } from "@angular/router";

import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
@Component({
  selector: "login",
  templateUrl: "./login.html",
  styleUrls: ["./login.scss"]
})
export class Login {
  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  constructor(public afService: AF, fb: FormBuilder, public router: Router) {
    this.form = fb.group({
      email: [
        "",
        Validators.compose([Validators.required, Validators.minLength(4)])
      ],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(4)])
      ]
    });

    this.email = this.form.controls["email"];
    this.password = this.form.controls["password"];
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      this.afService
        .loginWithEmail(this.email.value, this.password.value)
        .then(() => {
          this.afService.af
            .object("/users/" + firebase.auth().currentUser.uid)
            .subscribe(res => {
              if (res.role == 2) {
                this.router.navigate(["pages"]);
              } else {
                this.router.navigate(["pages"]);

                //alert('Unauthorized Access');
              }
            });
        })
        .catch((error: any) => {
          if (error) {
            alert(error);
          }
        });
    }
  }

  google() {
    this.afService.loginWithGoogle().then(data => {
      this.afService.af
        .object("/users/" + firebase.auth().currentUser.uid)
        .subscribe(res => {
          if (res.role == 2) {
            this.router.navigate(["pages"]);
          } else {
            alert("Unauthorized Access");
          }
        });
    });
  }

  facebook() {
    this.afService.loginWithFacebook().then(data => {
      this.afService.af
        .object("/users/" + firebase.auth().currentUser.uid)
        .subscribe(res => {
          if (res.role == 2) {
            this.router.navigate(["pages"]);
          } else {
            alert("Unauthorized Access");
          }
        });
    });
  }
}
