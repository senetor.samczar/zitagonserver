export const PAGES_MENU = [
  {
    path: "pages",
    children: [
      {
        path: "dashboard",
        data: {
          menu: {
            title: "general.menu.dashboard",
            icon: "ion-android-home",
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: "forms",
        data: {
          menu: {
            title: "Contents",
            icon: "ion-compose",
            selected: false,
            expanded: false,
            order: 400
          }
        },
        children: [
          {
            path: "inputsmood",
            data: {
              menu: {
                title: "Edit Mood"
              }
            }
          },
          {
            path: "inputstrack",
            data: {
              menu: {
                title: "Edit Song"
              }
            }
          },
          {
            path: "inputsalbum",
            data: {
              menu: {
                title: "Edit Album"
              }
            }
          },
          {
            path: "inputs",
            data: {
              menu: {
                title: "Edit Artist"
              }
            }
          },
          {
            path: "inputsregion",
            data: {
              menu: {
                title: "Edit Region"
              }
            }
          },
          {
            path: "inputscountry",
            data: {
              menu: {
                title: "Edit Country"
              }
            }
          },
          {
            path: "layouts",
            data: {
              menu: {
                title: "Add"
              }
            }
          }
        ]
      }
    ]
  }
];
