import { Injectable, Inject } from "@angular/core";
import { AngularFireModule } from "angularfire2";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
  FirebaseListObservable
} from "angularfire2/database";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import { FirebaseApp } from "angularfire2";

import * as firebase from "firebase";
import { Router } from "@angular/router";

@Injectable()
export class AF {
  public messages: FirebaseListObservable<any>;
  public albums: FirebaseListObservable<any>;
  public artists: FirebaseListObservable<any>;
  public tracks: FirebaseListObservable<any>;
  public countrys: FirebaseListObservable<any>;
  public regionss: FirebaseListObservable<any>;
  public tracksNum: any = 0;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;

  constructor(
    public af: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    public router: Router,
    @Inject(FirebaseApp) public fire: any
  ) {
    this.albums = this.af.list("albums");
    this.artists = this.af.list("artists");
    this.tracks = this.af.list("tracks");
    this.countrys = this.af.list("countrys");
    this.regionss = this.af.list("regionss");

    this.af.list("/tracks", { preserveSnapshot: true }).subscribe(snapshots => {
      this.tracksNum = snapshots.length;
    });
  }

  signOut(): void {
    firebase.auth().signOut();
    this.router.navigate(["login"]);
  }

  loginWithEmail(email, password) {
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch(function(error) {});
  }

  loginWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return firebase.auth().signInWithPopup(provider);
  }

  loginWithFacebook() {
    const provider = new firebase.auth.FacebookAuthProvider();
    return firebase.auth().signInWithPopup(provider);
  }

  getTodayStart() {
    const today1 = new Date();
    today1.setHours(0);
    today1.setMinutes(0);
    today1.setSeconds(0);
    return today1.valueOf();
  }

  getTodayEnd() {
    const today1 = new Date();

    return today1.valueOf();
  }

  getYesterdayStart() {
    let today1 = new Date();
    today1.setHours(0);
    today1.setMinutes(0);
    today1.setSeconds(0);
    today1 = new Date(today1.getTime() - 24 * 60 * 60 * 1000);

    return today1.valueOf();
  }

  getYesterdayEnd() {
    const today1 = new Date();

    return today1.valueOf();
  }

  getWeekStart() {
    let today1 = new Date();
    today1.setHours(0);
    today1.setMinutes(0);
    today1.setSeconds(0);
    today1 = new Date(today1.getTime() - 7 * 24 * 60 * 60 * 1000);
    return today1.valueOf();
  }

  getWeekEnd() {
    const today1 = new Date();

    return today1.valueOf();
  }

  getMonthStart() {
    let today1 = new Date();
    today1.setHours(0);
    today1.setMinutes(0);
    today1.setSeconds(0);
    today1 = new Date(today1.getTime() - 30 * 24 * 60 * 60 * 1000);

    return today1.valueOf();
  }

  getMonthEnd() {
    const today1 = new Date();

    return today1.valueOf();
  }
}
