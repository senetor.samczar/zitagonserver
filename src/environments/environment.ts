// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCS6_oV9mHIqhGAn2Xn6icfaNrzzJ-7fAY",
    authDomain: "zitagon-staging.firebaseapp.com",
    databaseURL: "https://zitagon-staging.firebaseio.com",
    projectId: "zitagon-staging",
    storageBucket: "zitagon-staging.appspot.com",
    messagingSenderId: "838875225403"
  }
};
